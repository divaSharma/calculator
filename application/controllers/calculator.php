<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Calculator extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	}
	
	public function index()
	{
		$this->load->view('header_view');
		$this->load->view('extras_view');
		$this->load->view('1.1/calc_view');
		$this->load->view('1.1/changeLog_view');
		$this->load->view('footer_view');
	}

	function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	public function mail($type) {
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name', 'name', 'required');
		$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email');
		if($type === "BugReport") {
			$this->form_validation->set_rules('type', 'type', 'required');
			$this->form_validation->set_rules('browser', 'browser', 'required');
		}
		$this->form_validation->set_rules('message', 'message', 'trim|required|xss_clean');


		if ($this->form_validation->run() == TRUE) {
			if($type === "Suggestion")
				$message = "From: ".$this->input->post('name')." <".$this->input->post('email').">\n\r Suggestion:".$this->input->post('message');
			else if($type === "BugReport")
				$message = "Bug Report for ".$this->input->post("type")." type on ".$this->input->post("platform")." platform for ".$this->input->post("browser")." browser\r\n Sender Mail: ".$this->input->post('email')."\r\n Message: ".$this->input->post('message');

			$subject = "$type for Angular Calc";
			self::send_mail('idiva.sharma@gmail.com', $subject, $message, "idiva.sharma@gmail.com");
			self::send_mail($this->input->post('email'), "Thank You", "Hi, \n\rThank you for the ".(($type === "BugReport") ? "Bug Report" : "Suggestion").". I will surely read and check and ".(($type === "BugReport") ? "Fix the bug" : "Implement it if I feel it's good suggestion. ")."Thank you! \n\t - \n\t Divya Sharma.", "idiva.sharma@gmail.com");
			
		} else {
			echo validation_errors();
			die();
		}
		$token = $this->generateRandomString();
		header('Location:'.base_url("?mail=success&token=$token"));
	}

	public static function send_mail($reciever, $sub, $text, $sender) {
		$to = $reciever;
		$subject = $sub;
		$message = $text;
		$headers = "From: Calc Visitor <$sender> \r\n";
		$headers .= "Reply-To: $sender \r\n";
		$headers .= "MIME-Version: 1.0 \r\n";
		$headers .= 'Content-Type: text/html; charset=ISO-8859-1'."\r\n";

		mail($to, $subject, $message, $headers);
	}
}