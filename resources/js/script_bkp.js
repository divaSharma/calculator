var app = angular.module('divya', []);
app.controller('sharma', ['$scope', function($scope) {

    $scope.setText = "0";
	$scope.points = [1,2,3,4,5,6,7,8,9];
	$scope.calculate = ["/","*","-","+"];
	
	var value1 = "",
		lclick = '';
	
	$scope.showText = function(item){
		($scope.setText == "0") && ($scope.setText = '');
		$scope.setText += item;
		$scope.lclick = 'number';
	};

    $scope.calc = function (op) {
        var operator = op,
			value = $scope.setText;
		if($scope.lclick != 'operator') {
			$scope.setText += operator;
			value1 = $scope.setText;
		} else {
			$scope.setText = value.slice(0, -1) + operator;
			value1 = $scope.setText;
		}
		$scope.lclick = 'operator';
    };

    $scope.answer = function () {
		value1 = $scope.setText;
		var chkIt = (value1.indexOf("+") != -1 || value1.indexOf("-") != -1 || value1.indexOf("*") != -1 || value1.indexOf("/") != -1);
		if(chkIt) {
			var arr = value1.replace(/\s/g, "").split(''),
				operand = "+",
				value = 0,
				result = 0;
			console.log(arr);
			$.each(arr, function(ind, val) {
				var me = $(this)[0];

				if($.isNumeric(me)) {
					value += me;
				}

				if(!($.isNumeric(me)) || (typeof(arr[ind + 1]) == "undefined")) {
					if(operand == "+")
						result = Number(result) + Number(value);
					else if(operand == "-")
						result = Number(result) - Number(value);
					else if(operand == "/")
						result = Number(result) / Number(value);
					else if(operand == "*")
						result = Number(result) * Number(value);
					operand = me;
					value = 0;
					$scope.setText = result;
				}

			});
		}
    };
	
	$scope.clearAll = function (){
		$scope.setText  = "0";
	}
}]);

$(document).ready(function() {
	function isNumberKey(evt) {
		var charCode = (evt.which) ? evt.which : event.keyCode;
		if (charCode != 46 && charCode > 31 
		&& (charCode < 48 || charCode > 57))
			return false;

		return true;
	}
	
	$(document).keypress(function(e) {
		if(isNumberKey(e)) {
			var scr = $("#screen");
			(scr.val() == "0") && (scr.val(""));
			scr.focus();
		}
	});
});