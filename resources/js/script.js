var app = angular.module('divya', []);

app.service('_deepCal', function() {
	var _ = this;
	_.a = function(a,b){return Number(a) + Number(b)};
	_.s = function(a,b){return Number(a) - Number(b)};
	_.m = function(a,b){return Number(a) * Number(b)};
	_.d = function(a,b){return Number(a) / Number(b)};
});

app.service('deepCal', ["_deepCal", function(ds) {
	this.cal = function(t) {
		var	r = t.replace(/\s/g, "").split(""), c = "+", n = 0, o = 0; $.each(r, function(i) {
			var s = $(this)[0],
				chk = ($.isNumeric(s) || s == "."),
				def = ("undefined" != typeof r[i + 1]);
			(chk) && (n += s);
			(chk && def) || ("+" == c ? o = ds.a(o, n) : "-" == c ? o = ds.s(o, n) : "/" == c ? o = ds.d(o, n) : "*" == c && (o = ds.m(o, n)), c = s, n = 0)
		});
		return o;
	};

	this.brackets = function(t) {
        for (flag = 0, j = 0, temp = " ", b = [], local = "", i = 0; i < t.length; i++) {
            if("(" == t[i] && 0 == flag++) {
                ($.isNumeric(t[i - 1]) ? local = "*" : (local = t[i - 1], temp = temp.substring(0, -1)), b[j] = temp, b[j + 1] = local, j += 2, temp = "")
            } else {
                ")" == t[i] && 1 == flag-- ? (b[j] = temp, j++, temp = "", $.isNumeric(t[i + 1]) ? (b[j] = "*", j++) : (i++, b[j] = t[i], j++)) : temp += t[i];
            }
        }
        return b.push(temp), b
    };
}]);

app.service("bodCal", ["deepCal", function(e) {
    function t(i, t) {
        var l = 0, a = i.replace(/\s/g, ""), n = a.split(""), r = (a.split(/[-+\/*]/g), 0); return $.each(t, function(i) {
            return $.each(n, function(s) {
                var u = 0, c = 0;
                if (n[s] == t[i]) {
                    for (lp1 = s - 1; lp1 > 0 && (u = $.isNumeric(n[lp1]) ? u : lp1 + 1, $.isNumeric(n[lp1])); lp1--);
                    for (lp2 = s + 1; lp2 <= n.length && (c = $.isNumeric(n[lp2]) ? u : lp2, $.isNumeric(n[lp2])); lp2++);
                    var o = e.cal(a.substring(u, c));
                    a = a.substring(0, u) + o + a.substring(c), r = 1, l = a
                }
                return 1 == r ? !1 : void 0
            }), 1 == r ? !1 : void 0
        }), l
    }

    function l(i) {
        var t = e.brackets("1+2(6/3)+9/(3*2(8/7)+2)4");
        console.log(t);
        var i = i.split(""), l = 0; return $.each(i, function(e) {
            ("/" == i[e] || "*" == i[e] || "+" == i[e] || "-" == i[e]) && l++
        }), l
    }

    this.calculateIt = function(e) {
        var a = e, n = ["/", "*", "+", "-"], r = l(e); for (i = 1; i <= r; i++) var a = t(a, n);
        return a
    }
}])

app.controller("rocks", ["$scope", "deepCal", "bodCal", function(diva, t, s) {
    diva.setText = "0", diva.points = [1, 2, 3, 4, 5, 6, 7, 8, 9], diva.calculate = ["/", "*", "-", "+"], diva.bodmas = !1, diva["class"] = "toggle", diva.records = [];
    var o = "";
    diva.showText = function(t) {
        "." === t && -1 != diva.setText.indexOf(".") && (diva.setText = diva.setText.replace(/\./g, "")), "0" == diva.setText && "." !== t && (diva.setText = ""), diva.setText = diva.setText + "" + t, diva.lclick = "number"
    }, diva.calc = function(t) {
        var s = t,
            l = diva.setText;
        "operator" != diva.lclick ? (diva.setText += s, o = diva.setText) : (diva.setText = l.slice(0, -1) + s, o = diva.setText), diva.lclick = "operator"
    }, diva.answer = function() {
        if (o = diva.setText, o.indexOf("Infinity") > -1) return diva.setText = "Infinity", !1;
        o = $.isNumeric(o.substring(o.length - 1)) ? o : o.substring(0, o.length - 1);
        var l = diva.bodmas ? s.calculateIt(o) : t.cal(o);
        if (diva.setText = l, o.split(/[-+\/*]/g).length > 1) {
            var c = 0;
            $.each(diva.records, function(e, t) {
                $.each(t, function(e, t) {
                    t == o && (c = 1)
                })
            }), 0 == c && diva.records.push({
                formulae: o,
                type: 1 == diva.bodmas ? "bodmas" : "",
                answer: l
            })
        }
    }, diva.clearAll = function() {
        diva.setText = "0"
    }, diva.clear = function() {
        var t = diva.setText;
        diva.setText = t.length > 1 ? t.substring(0, t.length - 1) : 0
    }, diva.bodTog = function() {
        diva["class"] = "toggle" == diva["class"] ? "toggle active" : "toggle", diva.bodmas = 1 == diva.bodmas ? !1 : !0
    }, diva.reCalc = function(t) {
        var s = t.split("&");
        diva["class"] = "bodmas" == s[1] ? "toggle active" : "toggle", diva.bodmas = "bodmas" == s[1] ? !0 : !1, diva.setText = s[0]
    }, diva.cleanHist = function() {
        diva.records = []
    }
}]);

$(document).ready(function() {
    function i(e) {
        var n, o;
        window.event ? (n = window.event.keyCode, o = !!window.event.shiftKey) : (n = ev.which, o = !!ev.shiftKey);
        var i = e.which ? e.which : event.keyCode;
        return console.log(i >= 48 && 57 >= i), o ? 43 == i || 187 == i || 56 == i ? 1 : 0 : i >= 48 && 57 >= i || 46 == i || 45 == i || 189 == i || 191 == i ? 1 : 0
    }
    function makenumber(n) {
        return 1 == n ? "one" : 2 == n ? "two" : 3 == n ? "three" : 4 == n ? "four" : 5 == n ? "five" : 6 == n ? "six" : 7 == n ? "seven" : 8 == n ? "eight" : 9 == n ? "nine" : 10 == n ? "ten" : void 0
    }
    function placenumber() {
        var n = Math.floor(10 * Math.random() + 1),
            t = Math.floor(10 * Math.random() + 1);
        no1 = makenumber(n), no2 = makenumber(t), ans = n + t, $(".answer").attr("data-pattern", ans), $(".no1").html(no1), $(".no2").html(no2)
    }
    $first = !0, $(document).keypress(function(e) {
        if (i(e)) {
            var t = $("#screen");
            "0" == t.val() && t.val(""), t.focus()
        }
    }), $("#screen").keydown(function(e) {
        !i(e) && e.preventDefault();
        var t = $(this).val();
        190 == e.which && -1 != t.indexOf(".") && $(this).val(t.replace(/\./g, ""))
    }), $(".info .list a.heading").click(function(i) {
        i.preventDefault();
        var e = $(this).attr("data-ver"),
            t = $(".info .list a.heading"),
            l = $(".info .details ul");
        t.removeClass("active"), l.css("display", "none"), $(this).addClass("active"), $.each(l, function() {
            $(this).attr("data-log") == e && $(this).css("display", "block")
        })
    }), $(".sub_nav ul.sub_menu li").click(function() {
        var i = $(this), e = i.attr("data-link"), t = "none", l = "block", a = "display", s = "active";
        $.each($(".sub_nav ul.sub_menu li"), function() {
            var i = $(this);
            i.attr("data-link") != e ? i.removeClass(s) : i.hasClass(s) ? i.removeClass(s) : i.addClass(s)
        }), $.each($(".sub_nav_items ul.items li.item"), function() {
            var i = $(this);
            i.attr("data-bind") == e ? i.css(a, i.is(":visible") ? t : l) : i.css(a, t)
        })
    }), $(".ng-binding").click(function() {
        "=" === $(this).text() && $first && ($first = !1, $(".main .nopadding:last-child").css("margin", "0px"), "337px" === $(".calc_section .records").css("height") ? $(".calc_section .records").show("slide", {
            direction: "right"
        }) : $(".calc_section .records").show("slide", {
            direction: "down"
        }))
    }), jQuery("ul.select-box").on("click", "li.active", function() {
        var i = $(this).parents(".select-box").attr("alt");
        $.each($(".select-box"), function() {
            $(this).attr("alt") != i && $(this).find("ul.menu").css("display", "")
        });
        var e = jQuery(this), t = e.parents(".select-box").find("ul.menu"), l = t.is(":visible");
        t.css("display", l ? "none" : "block")
    }), jQuery("ul.select-box").on("click", "ul.menu li", function() {
        var i = $(this), e = i.text(), t = i.parents(".sub_nav_items").find(".select-box");
        jQuery(this).parents(".select-box").find("li.active span").text(e), jQuery(this).parents("ul.menu").css("display", "none");
        var l = "<li value='Android'>Android</li><li value='Windows'>Windows</li><li value='iOS'>iOS</li><li value='Blackberry'>Blackberry</li>",
            a = "<li value='Win7'>Windows 7</li><li value='Win8'>Windows 8</li><li value='Win10'>Windows 10</li><li value='Mac'>Mac</li><li value='Linux'>Linux</li>",
            s = "<li value='chrome'>chrome</li><li value='firefox'>firefox</li><li value='UC'>UC Browser</li><li value='Opera'>Opera Mini</li><li value='Default'>Default Browser</li>",
            n = "<li value='chrome'>chrome</li><li value='firefox'>firefox</li><li value='UC'>UC Browser</li><li value='Opera'>Opera Mini</li><li value='Safari'>Safari</li>",
            r = "<li value='chrome'>chrome</li><li value='firefox'>firefox</li><li value='Opera'>Opera</li><li value='IE'>Internet Explorer 9 & above</li><li value='Safari'>Safari</li>";
        "Mobile" == e ? $.each(t, function() {
            "type" == $(this).attr("alt") && $(this).find(".menu").html(l), "browser" == $(this).attr("alt") && $(this).find(".menu").html(s)
        }) : "Computer" == e ? $.each(t, function() {
            "type" == $(this).attr("alt") && $(this).find(".menu").html(a), "browser" == $(this).attr("alt") && $(this).find(".menu").html(r)
        }) : "Android" == e || "Windows" == e || "Blackberry" == e ? $.each(t, function() {
            "browser" == $(this).attr("alt") && $(this).find(".menu").html(s)
        }) : "iOS" == e && $.each(t, function() {
            "browser" == $(this).attr("alt") && $(this).find(".menu").html(n)
        }), i.parents(".select-box").parent().find('input[type="hidden"]').val(e)
    }), $(".sub_nav_items .items input[type='button']").click(function() {
        var i = $(this).attr("value");
        if ("Cancel" == i) {
            var e = $(this).parents(".item");
            e.css("display", ""), e.find('input[type="text"],input[type="hidden"],textarea').val(""), e.find(".select-box .active span").text("select"), $(".sub_nav ul.sub_menu li").removeClass("active")
        }
    }), $(".sub_nav_items form").submit(function(n) {
        var t = $(this).find(".answer");
        t.val() != t.attr("data-pattern") && (n.preventDefault(), alert("Oops! Answer is not correct"))
    }), placenumber();
});